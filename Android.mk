
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

#LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := LibusbJava/LibusbJava.cpp

	


LOCAL_C_INCLUDES += $(LOCAL_PATH)/android \
	external/libusb-compat/libusb \
	$(JNI_H_INCLUDE)

LOCAL_CFLAGS += -W -Wall
LOCAL_CFLAGS += -fPIC -DPIC


LOCAL_SHARED_LIBRARIES := libusb-compat


ifeq ($(TARGET_BUILD_TYPE),release)
	LOCAL_CFLAGS += -O2
endif

LOCAL_MODULE:= libusbJava

LOCAL_PRELINK_MODULE := false 
include $(BUILD_SHARED_LIBRARY)
